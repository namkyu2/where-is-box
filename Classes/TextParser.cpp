#include "TextParser.h"
#include <fstream>
#include <stdio.h>
#include <iostream>

using namespace std;

int TextParser::TextRead(string filename)
{
	ifstream OpenFile(filename);

	char ch;
	while (!OpenFile.eof())
	{
		OpenFile.get(ch);
		cout << ch;
	}
	OpenFile.close();

	return 0;
}