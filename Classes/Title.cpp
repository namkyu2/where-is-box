#include "Title.h"
#include "Update.h"

using namespace cocos2d;

class TitleLayer : public Layer
{
public:
	CREATE_FUNC(TitleLayer);

	Menu* pMenu;
	MenuItem* pMenuItem1;
	Vec2 origin;
	Size visibleSize;

	virtual bool init() override
	{
		if (!Layer::init()) return false;

		origin = Director::getInstance()->getVisibleOrigin();
		visibleSize = Director::getInstance()->getVisibleSize();
		auto label = Label::createWithTTF("Where is Box", "fonts/Marker Felt.ttf", 40);
		label->setPosition(Vec2(origin.x + visibleSize.width / 2,
			origin.y + visibleSize.height - label->getContentSize().height));
		this->addChild(label, 1);
		pMenuItem1 = MenuItemImage::create(
			"Menu_Start_Normal.png",
			"Menu_Start_Pushed",
			this,
			menu_selector(TitleLayer::menucallfirst));

		pMenu = Menu::create(pMenuItem1, NULL);
		pMenu->alignItemsVertically();
		this->addChild(pMenu, 1);

		return true;
	}

	void TitleLayer::menucallfirst(CCObject *pSender)
	{
		CCScene* pScene = Update::create();
		CCTransitionFade * trans = CCTransitionFade::create(1.0f, pScene);
		CCDirector::sharedDirector()->pushScene(trans);
	}
};

bool Title::init()
{
	if (!Scene::init()) return false;
	this->addChild(TitleLayer::create());

	return true;
}