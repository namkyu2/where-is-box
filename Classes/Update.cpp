#include "Update.h"
#include "Object\Ground.h"
#include "Object\Hero.h"
#include "Stage\Stage1.h"

using namespace cocos2d;

class UpdateLayer : public Layer
{
public:
	CREATE_FUNC(UpdateLayer);
	int StageCount;
	int mapnum;
	int now_heronum;
	int now_mapnum;
	bool x_push;
	bool text1;
	bool text1off;
	bool text2;
	bool text2off;
	bool text3_first;
	bool text3_second;
	bool text3off;
	bool text4_first;
	bool text4_second;
	bool text4off;
	bool text5;
	bool meetbighero;
	Hero* hero;
	Hero* bighero;
	Stage1* stage1;
	CCArray* heroarray;
	CCArray* groundarray;
	Ground* ground;
	boolean rightpress;
	boolean leftpress;
	Rect MoveRight;
	Label* pLabel;
	Label* pLabel2;

	virtual bool init()
	{
		if (!Layer::init())
			return false;

		StageCount = 1;
		mapnum = 1;
		now_heronum = 1;
		now_mapnum = 1;
		x_push = false;
		text1 = false;
		text1off = false;
		text2 = false;
		text2off = false;
		text3_first = false;
		text3_second = false;
		text3off = false;
		text4_first = false;
		text4_second = false;
		text4off = false;
		text5 = false;
		meetbighero = false;
		hero = Hero::create(Rect(0, 0, 30, 25));
		hero->setPosition(Point(150, 300));
		addChild(hero);
		heroarray = CCArray::create();
		heroarray->retain();
		heroarray->addObject(hero);

		auto listener = EventListenerKeyboard::create();
		listener->onKeyPressed = CC_CALLBACK_2(UpdateLayer::onKeyPressed, this);
		listener->onKeyReleased = CC_CALLBACK_2(UpdateLayer::onKeyReleased, this);

		_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

		scheduleUpdate();

		return true;
	}

	virtual void update(float deltaTime) override
	{
		if (StageCount == 1)
		{
			stage1 = Stage1::create();
			addChild(stage1);
			groundarray = stage1->groundarray;
			hero->HeroFadeIn(1.0f);
			StageCount = -1;
		}
		if (StageCount == -1)
		{
			CCObject* obj1 = NULL;

			CCARRAY_FOREACH(heroarray, obj1)
			{
				Hero* temp_hero = (Hero*)obj1;

				if ((temp_hero->getHeroArea()).intersectsRect(temp_hero->getHeroRightBox()))
				{
					GroundAllMoveLeft(stage1->getGroundArray());
					GroundAllMoveLeft(stage1->getGoalArray());
					if (temp_hero->Getheronumber() == 1)
						temp_hero->setPosition(Point(50, 200));
					else if (temp_hero->Getheronumber() == 2)
						temp_hero->setPosition(Point(50, 200));
					temp_hero->SetResetPoint(Point(100, 300));
					temp_hero->Setmapnumber(temp_hero->Getmapnumber() + 1);
					now_mapnum = temp_hero->Getmapnumber();
					CCObject* obj2 = NULL;
					CCARRAY_FOREACH(heroarray, obj2)
					{
						Hero* temp_hero = (Hero*)obj2;
						if (temp_hero->Getheronumber() != now_heronum)
						{
							temp_hero->setPositionX(temp_hero->getPositionX() - 1280);
						}
					}
				}
				if ((temp_hero->getHeroArea()).intersectsRect(temp_hero->getHeroLeftBox()))
				{
					GroundAllMoveRight(stage1->getGroundArray());
					GroundAllMoveRight(stage1->getGoalArray());
					if (temp_hero->Getheronumber() == 1)
						temp_hero->setPosition(Point(1240, 200));
					else if (temp_hero->Getheronumber() == 2)
						temp_hero->setPosition(Point(1150, 200));

					temp_hero->SetResetPoint(Point(1140, 300));
					temp_hero->Setmapnumber(temp_hero->Getmapnumber() - 1);
					now_mapnum = temp_hero->Getmapnumber();
					CCObject* obj2 = NULL;
					CCARRAY_FOREACH(heroarray, obj2)
					{
						Hero* temp_hero = (Hero*)obj2;
						if (temp_hero->Getheronumber() != now_heronum)
						{
							temp_hero->setPositionX(temp_hero->getPositionX() + 1280);
						}
					}
				}

				//첫번째 텍스트
				if (now_mapnum == 1)
				{
					if (!text1)
					{
						if (temp_hero->getPositionX() > 200)
						{
							pLabel = Label::createWithTTF("Huh? Where Am I", "fonts/Marker Felt.ttf", 40);
							pLabel->setPosition(Point(800, 400));
							pLabel->setColor(Color3B(255, 255, 255));
							addChild(pLabel);
							pLabel->setOpacity(0);
							auto fadeIn = FadeIn::create(1.0f);
							pLabel->runAction(fadeIn);
							text1 = true;
						}
					}
				}

				//두번째 텍스트 : 점프 튜토리얼
				if (now_mapnum == 2)
				{
					if (!text2)
					{
						if (temp_hero->getPositionX() > 120)
						{
							pLabel = Label::createWithTTF("Z, Jump", "fonts/Marker Felt.ttf", 40);
							pLabel->setPosition(Point(400, 500));
							pLabel->setColor(Color3B(255, 255, 255));
							addChild(pLabel);
							pLabel->setOpacity(0);
							auto fadeIn = FadeIn::create(1.0f);
							pLabel->runAction(fadeIn);
							text2 = true;
						}
					}
				}

				//세번째 텍스트 2개 출력 : Mr.Box 점프 가능
				if (now_mapnum == 3)
				{
					if (temp_hero->getPositionX() > 120)
					{
						if (!text3_first)
						{
							pLabel = Label::createWithTTF("Mr.Box can jump in the air", "fonts/Marker Felt.ttf", 40);
							pLabel->setPosition(Point(400, 500));
							pLabel->setColor(Color3B(255, 255, 255));
							addChild(pLabel);
							pLabel->setOpacity(0);
							auto fadeIn = FadeIn::create(1.0f);
							pLabel->runAction(fadeIn);
							text3_first = true;
						}
					}

					if (temp_hero->getPositionX() > 520)
					{
						if (!text3_second)
						{
							pLabel2 = Label::createWithTTF("But only once", "fonts/Marker Felt.ttf", 40);
							pLabel2->setPosition(Point(800, 300));
							pLabel2->setColor(Color3B(255, 255, 255));
							addChild(pLabel2);
							pLabel2->setOpacity(0);
							auto fadeIn = FadeIn::create(1.0f);
							pLabel2->runAction(fadeIn);
							text3_second = true;
						}
					}
				}

				//Mr.BigBox 등장 + 네번째 텍스트 2개 출력 
				if (now_mapnum == 4)
				{
					if (!meetbighero)
					{
						if (temp_hero->getPositionX() > 250)
						{
							bighero = Hero::create(Rect(0, 0, 80, 70));
							bighero->setPosition(Point(450, 200));
							addChild(bighero);
							heroarray->addObject(bighero);
							bighero->HeroFadeIn(1.0f);
							bighero->Setmapnumber(hero->Getmapnumber());
							bighero->Setheronumber(2);
							meetbighero = true;
						}
					}
					if (!text4_first)
					{
						if (temp_hero->getPositionX() > 250)
						{
							pLabel = Label::createWithTTF("Hi, Mr.BigBox", "fonts/Marker Felt.ttf", 40);
							pLabel->setPosition(Point(250, 330));
							pLabel->setColor(Color3B(255, 255, 255));
							addChild(pLabel);
							pLabel->setOpacity(0);
							auto fadeIn = FadeIn::create(1.0f);
							pLabel->runAction(fadeIn);
							text4_first = true;
						}
					}
					if (!text4_second)
					{
						if (temp_hero->Getheronumber() == now_heronum)
						{
							if (temp_hero->getPositionX() > 400)
							{
								pLabel2 = Label::createWithTTF("X, Change Box", "fonts/Marker Felt.ttf", 40);
								pLabel2->setPosition(Point(600, 440));
								pLabel2->setColor(Color3B(255, 255, 255));
								addChild(pLabel2);
								pLabel2->setOpacity(0);
								auto fadeIn = FadeIn::create(1.0f);
								pLabel2->runAction(fadeIn);
								text4_second = true;
							}
						}
					}
				}

				//첫번째 텍스트 삭제
				if (text1 && !text1off)
				{
					if (now_mapnum != 1)
					{
						this->removeChild(pLabel);
						text1off = true;
					}
				}

				//두번째 텍스트 삭제
				if (text2 && !text2off)
				{
					if (now_mapnum != 2)
					{
						this->removeChild(pLabel);
						text2off = true;
					}
				}

				//세번째 텍스트 2개 삭제
				if (text3_first && text3_second && !text3off)
				{
					if (now_mapnum != 3)
					{
						this->removeChild(pLabel);
						this->removeChild(pLabel2);
						text3off = true;
					}
				}

				//네번째 텍스트 2개 삭제
				if (text4_first && text4_second && !text4off)
				{
					if (now_mapnum != 4)
					{
						this->removeChild(pLabel);
						this->removeChild(pLabel2);
						text4off = true;
					}
				}
			}
		}	

		CCObject* obj2 = NULL;

		CCARRAY_FOREACH(heroarray, obj2)
		{
			Hero* temp_hero = (Hero*)obj2;

			temp_hero->applyForce(Point(0, -200.0f));
			if (temp_hero->Getheronumber() == now_heronum)
			{
				if (rightpress == true)
					temp_hero->KeyboardMove(1);
				if (leftpress == true)
					temp_hero->KeyboardMove(-1);
			}
			
			temp_hero->updatePhysics(temp_hero, heroarray, groundarray, deltaTime);

			if (temp_hero->getPositionY() < -50.0f)
			{
				temp_hero->setPosition(temp_hero->GetResetPoint());
				temp_hero->SetVelocityY(0);
				temp_hero->SetAccelerationY(0);
				temp_hero->HeroFadeIn(1.0f);
			}
		}

		Layer::update(deltaTime);
	}

	void onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
	{
		if (keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW)
		{
			rightpress = true;
		}

		if (keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW)
		{
			leftpress = true;
		}

		if (keyCode == EventKeyboard::KeyCode::KEY_ESCAPE)
		{
			Director::getInstance()->popScene();
		}

		if (keyCode == EventKeyboard::KeyCode::KEY_R)
		{
			CCObject* obj3 = NULL;
			CCARRAY_FOREACH(heroarray, obj3)
			{
				Hero* temp_hero = (Hero*)obj3;
				if (temp_hero->Getheronumber() == now_heronum)
				{
					temp_hero->setPosition(temp_hero->GetResetPoint());
					temp_hero->resetVelocity();
					temp_hero->resetAcceleration();
				}
			}
		}

		
		if (keyCode == EventKeyboard::KeyCode::KEY_Z)
		{
			//Z를 누르면 점프
			CCObject* obj4 = NULL;
			CCARRAY_FOREACH(heroarray, obj4)
			{
				Hero* temp_hero = (Hero*)obj4;
				if (temp_hero->Getheronumber() == now_heronum)
				{
					if (!(temp_hero->GetIsJump()))
					{
						if (temp_hero->Getheronumber() == 1)
						{
							temp_hero->SetVelocityY(1300.0f);
							temp_hero->SetAccelerationY(-6000.0f);
							temp_hero->SetIsJump(true);
						}
						else if (temp_hero->Getheronumber() == 2)
						{
							temp_hero->SetVelocityY(1600.0f);
							temp_hero->SetAccelerationY(-6000.0f);
							temp_hero->SetIsJump(true);
						}
						
					}
				}				
			}
		}

		if (keyCode == EventKeyboard::KeyCode::KEY_X)
		{
			//X를 누르면 박스 변경, 박스 변경 될 때 Ground 및 다른 박스들의 위치 변경을 해줘야함
			rightpress = false;
			leftpress = false;
			if (meetbighero)
			{
				int old_heronum = 0;
				int old_mapposition = 0;
				int basis_location = 0;

				CCObject* obj3 = NULL;
				CCARRAY_FOREACH(heroarray, obj3)
				{
					Hero* temp_hero = (Hero*)obj3;
					temp_hero->XresetVelocity();
				}

				CCObject* obj = NULL;
				CCARRAY_FOREACH(heroarray, obj)
				{
					Hero* temp_hero = (Hero*)obj;
					if (now_heronum == temp_hero->Getheronumber())
					{
						old_mapposition = temp_hero->Getmapnumber();
					}
				}

				if (now_heronum == 1)
				{
					old_heronum = now_heronum;
					now_heronum = 2;
				}
				else if (now_heronum == 2)
				{
					old_heronum = now_heronum;
					now_heronum = 1;
				}

				//바뀐 박스의 맵 위치를 받아옴
				CCObject* obj1 = NULL;
				CCARRAY_FOREACH(heroarray, obj1)
				{
					Hero* temp_hero = (Hero*)obj1;
					if (now_heronum == temp_hero->Getheronumber())
					{
						basis_location = temp_hero->Getmapnumber();
					}
				}

				int difference = old_mapposition - basis_location;

				CCObject* obj2 = NULL;
				CCARRAY_FOREACH(heroarray, obj2)
				{
					Hero* temp_hero = (Hero*)obj2;
					if (difference > 0)
					{
						for (int i = 1; i <= difference; i++)
						{
							HeroMoveRight(temp_hero);
						}
					}
					if (difference < 0)
					{
						for (int i = 1; i <= -difference; i++)
						{
							HeroMoveLeft(temp_hero);
						}
					}
				}

				int difference2 = old_mapposition - basis_location;

				if (difference > 0)
				{
					for (int i = 1; i <= difference; i++)
					{
						GroundAllMoveRight(groundarray);
						GroundAllMoveRight(stage1->getGoalArray());
					}
				}
				if (difference < 0)
				{
					for (int i = 1; i <= -difference; i++)
					{
						GroundAllMoveLeft(groundarray);
						GroundAllMoveLeft(stage1->getGoalArray());
					}
				}
			}
		}
	}

	void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
	{
		if (keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW)
		{
			rightpress = false;
			CCObject* obj3 = NULL;
			CCARRAY_FOREACH(heroarray, obj3)
			{
				Hero* temp_hero = (Hero*)obj3;
				temp_hero->XresetVelocity();
			}
			x_push = false;
		}

		if (keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW)
		{
			leftpress = false;
			CCObject* obj3 = NULL;
			CCARRAY_FOREACH(heroarray, obj3)
			{
				Hero* temp_hero = (Hero*)obj3;
				temp_hero->XresetVelocity();
			}
			x_push = false;
		}
	}

	void GroundAllMoveLeft(CCArray* objectarray)
	{
		CCObject* obj1 = NULL;

		CCARRAY_FOREACH(objectarray, obj1)
		{
			Ground* ground = (Ground*)obj1;
			ground->setPositionX(ground->getPositionX() - 1280);
		}
	}

	void GroundAllMoveRight(CCArray* objectarray)
	{
		CCObject* obj1 = NULL;

		CCARRAY_FOREACH(objectarray, obj1)
		{
			Ground* ground = (Ground*)obj1;
			ground->setPositionX(ground->getPositionX() + 1280);
		}
	}

	void HeroMoveLeft(Hero* object)
	{
		object->setPositionX(object->getPositionX() - 1280);
	}

	void HeroMoveRight(Hero* object)
	{
		object->setPositionX(object->getPositionX() + 1280);
	}
};

bool Update::init()
{
	if (!Scene::init()) return false;
	addChild(UpdateLayer::create());
	return true;
};