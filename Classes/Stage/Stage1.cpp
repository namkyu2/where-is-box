#include "Stage\Stage1.h"
#include "Object\Ground.h"
#include "TextParser.h"

using namespace cocos2d;

bool Stage1::init()
{
	if (!Layer::init())
		return false;

	TextParser text_read;
	int check = 1;
	//check = text_read.TextRead("Maps/test.txt");
	
	groundarray = CCArray::create();
	goalArray = CCArray::create();
	groundarray->retain();
	goalArray->retain();
	addChild(ground = Ground::create(Rect(0, 0, 1280, 200)));
	groundarray->addObject(ground);
	addChild(ground = Ground::create(Rect(0, 0, 50, 1200)));
	groundarray->addObject(ground);
	addChild(ground = Ground::create(Rect(1280, 0, 300, 200)));
	groundarray->addObject(ground);
	addChild(ground = Ground::create(Rect(1630, 0, 300, 200)));
	groundarray->addObject(ground);
	addChild(ground = Ground::create(Rect(1980, 0, 800, 200)));
	groundarray->addObject(ground);
	Casstlefloor = Ground::create(Rect(2880, 0, 2000, 200));
	groundarray->addObject(Casstlefloor);
	addChild(Casstlefloor);
	Casstle = Ground::create(Rect(3740, 300, 200, 800));
	groundarray->addObject(Casstle);
	addChild(Casstle);
	addChild(ground = Ground::create(Rect(4960, 0, 800, 300)));
	groundarray->addObject(ground);

	Sprite* goal_first = Sprite::create("Goal.png");
	Sprite* goal_second = Sprite::create("Goal.png");
	goal_first->setScaleX(30 / goal_first->getContentSize().width);
	goal_first->setScaleY(25 / goal_first->getContentSize().height);

	goal_first->setPosition(5400, 300);
	goal_first->setAnchorPoint(Point(0, 0));
	goal_second->setPosition(5500, 300);
	goal_second->setAnchorPoint(Point(0, 0));

	goal_second->setScaleX(80 / goal_second->getContentSize().width);
	goal_second->setScaleY(70 / goal_second->getContentSize().height);

	addChild(goal_first);
	addChild(goal_second);

	goalArray->addObject(goal_first);
	goalArray->addObject(goal_second);

	return true;
}

CCArray* Stage1::getGroundArray()
{
	return groundarray;
}

CCArray* Stage1::getGoalArray()
{
	return goalArray;
}