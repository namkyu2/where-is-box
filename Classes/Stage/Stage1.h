#pragma once

#include "cocos2d.h"

class Hero;
class Ground;

class Stage1 : public cocos2d::Layer
{
public:
	cocos2d::CCArray* groundarray;
	cocos2d::CCArray* goalArray;
	Ground* ground;
	Ground* Casstlefloor;
	Ground* Casstle;
	virtual bool init();
	cocos2d::CCArray* getGroundArray();
	cocos2d::CCArray* getGoalArray();
	CREATE_FUNC(Stage1);

};