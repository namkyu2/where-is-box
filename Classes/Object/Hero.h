#pragma once

#include "cocos2d.h"

class Ground;

class Hero : public cocos2d::Node
{
public:
	virtual bool init(const cocos2d::Rect& area);
	static Hero* create(const cocos2d::Rect& area);
	void HeroFadeIn(float time);
	int Getmapnumber();
	void Setmapnumber(int num);
	int Getheronumber();
	void Setheronumber(int num);
	cocos2d::Point GetResetPoint();
	void SetResetPoint(cocos2d::Point reset);
	void XresetVelocity();
	void resetVelocity();
	void resetAcceleration();
	void SetVelocityY(float velo);
	void SetAccelerationY(float acc);
	void KeyboardMove(int direction);
	void SetIsJump(boolean setting);
	bool GetIsJump();
	void applyForce(const cocos2d::Point& force);
	void updatePhysics(Hero* hero, cocos2d::CCArray* heroarray, cocos2d::CCArray* groundarray, float deltaTime);
	void ColideCheck_Ground(Hero* hero, const Ground* ground);
	void ColideCheck_Hero(Hero* hero, Hero* hero2);
	cocos2d::Rect getHeroArea() const;
	cocos2d::Rect getHeroRightBox() const;
	cocos2d::Rect getHeroLeftBox() const;

private:
	cocos2d::Sprite* sprite;
	cocos2d::Point velocity;
	cocos2d::Point acceleration;
	cocos2d::Point ResetPoint;
	cocos2d::Rect RightBox;
	cocos2d::Rect LeftBox;
	int mapnumber;
	int heronumber;
	float mass;
	boolean IsJump;
};