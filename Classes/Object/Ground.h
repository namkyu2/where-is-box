#pragma once

#include "cocos2d.h"

class Ground : public cocos2d::Node
{
public:
	virtual bool init(const cocos2d::Rect& area);
	static Ground* create(const cocos2d::Rect& area);


	cocos2d::Rect getGroundArea() const;

private:
	cocos2d::Sprite* sprite;
};