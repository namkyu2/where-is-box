#include "Object\Hero.h"
#include "Object\Ground.h"

using namespace cocos2d;

bool Hero::init(const Rect& area)
{
	if (!Node::init()) return false;

	sprite = Sprite::create("Object_Hero.png");
	sprite->setPosition(Point::ZERO);
	sprite->setAnchorPoint(Point::ZERO);
	sprite->setScaleX(area.size.width / sprite->getContentSize().width);
	sprite->setScaleY(area.size.height / sprite->getContentSize().height);
	sprite->setOpacity(0);
	addChild(sprite);

	setContentSize(area.size);
	setPosition(area.origin);

	mapnumber = 1;
	heronumber = 1;
	velocity = Point::ZERO;
	acceleration = Point::ZERO;
	mass = 1.0f;
	RightBox = Rect(1280, 0, 2, 1000);
	LeftBox = Rect(0, 0, 2, 1000);
	IsJump = true;
	ResetPoint = Point(150, 300);
	return true;
}

Hero* Hero::create(const Rect& area)
{
	Hero* ret = new Hero();
	if ((ret != nullptr) && ret->init(area))
	{
		ret->autorelease();
	}
	else
		CC_SAFE_RELEASE_NULL(ret);
	return ret;
}

void Hero::HeroFadeIn(float time)
{
	this->sprite->setOpacity(0);
	auto fadeIn = FadeIn::create(time);
	this->sprite->runAction(fadeIn);
}

int Hero::Getmapnumber()
{
	return mapnumber;
}

void Hero::Setmapnumber(int num)
{
	mapnumber = num;
}

int Hero::Getheronumber()
{
	return heronumber;
}

void Hero::Setheronumber(int num)
{
	heronumber = num;
}

Point Hero::GetResetPoint()
{
	return ResetPoint;
}

void Hero::SetResetPoint(Point reset)
{
	ResetPoint = reset;
}

void Hero::XresetVelocity()
{
	velocity.x = 0;
}

void Hero::resetVelocity()
{
	velocity = Point(0, 0);
}

void Hero::resetAcceleration()
{
	acceleration = Point(0, 0);
}

void Hero::SetVelocityY(float velo)
{
	velocity.y = velo;
}

void Hero::SetAccelerationY(float acc)
{
	acceleration.y = acc;
}

void Hero::SetIsJump(boolean setting)
{
	IsJump = setting;
}

bool Hero::GetIsJump()
{
	return IsJump;
}

void Hero::KeyboardMove(int direction)
{
	if (direction == 1)
	{
		velocity.x = 200.0f;
	}
	else if (direction == -1)
	{
		velocity.x = -200.0f;
	}
}

void Hero::applyForce(const Point& force)
{
	acceleration += force / mass;
}

void Hero::updatePhysics(Hero* hero, CCArray* heroarray, CCArray* groundarray, float deltaTime)
{
	CCObject* obj1 = NULL;
	CCObject* obj2 = NULL;
	Point position = hero->getPosition();
	hero->velocity += acceleration * deltaTime;
	hero->setPosition(hero->getPosition() + velocity * deltaTime);

	CCARRAY_FOREACH(groundarray, obj1)
	{
		Ground* ground = (Ground*)obj1;
		ColideCheck_Ground(hero, ground);
	}

	CCARRAY_FOREACH(heroarray, obj2)
	{
		Hero* temp_hero = (Hero*)obj2;
		if (temp_hero->Getheronumber() > hero->Getheronumber())
			ColideCheck_Hero(hero, temp_hero);
	}

}	


void Hero::ColideCheck_Ground(Hero* hero, const Ground* ground)
{
	Rect hero_temp = getHeroArea();
	Rect ground_temp = ground->getGroundArea();
	if (hero_temp.intersectsRect(ground_temp))
	{
		if (hero->getPositionY() > ground->getPositionY())
		{
			if ((hero->getPositionX() - ground->getPositionX()) < (ground->getContentSize().width - hero->getContentSize().width/2) && (hero->getPositionX() - ground->getPositionX()) > -(hero->getContentSize().width/2))
			{
				velocity.y = 0;
				acceleration.y = 0;
				hero->setPositionY(ground->getPositionY() + ground->getContentSize().height);
				IsJump = false;
			}
			else if ((hero->getPositionX() - ground->getPositionX()) < -(hero->getContentSize().width/2))
			{
				acceleration.x = 0;
				hero->setPositionX(ground->getPositionX() - (hero->getContentSize().width));
			}
			else if ((hero->getPositionX() - ground->getPositionX()) >(ground->getContentSize().width - hero->getContentSize().width / 2))
			{
				acceleration.x = 0;
				hero->setPositionX(ground->getPositionX() + ground->getContentSize().width);
			}
		}
		else
		{
			if ((hero->getPositionX() - ground->getPositionX()) < (ground->getContentSize().width - hero->getContentSize().width / 2) && (hero->getPositionX() - ground->getPositionX()) > -(hero->getContentSize().width / 2))
			{
				velocity.y = -velocity.y;
				hero->setPositionY(ground->getPositionY()-hero->getContentSize().height);
			}
			else if ((hero->getPositionX() - ground->getPositionX()) < -(hero->getContentSize().width / 2))
			{
				acceleration.x = 0;
				hero->setPositionX(ground->getPositionX() - (hero->getContentSize().width));
			}
			else if ((hero->getPositionX() - ground->getPositionX()) >(ground->getContentSize().width - hero->getContentSize().width / 2))
			{
				acceleration.x = 0;
				hero->setPositionX(ground->getPositionX() + ground->getContentSize().width);
			}
		}
	}
}

void Hero::ColideCheck_Hero(Hero* hero, Hero* hero2)
{
	Rect hero_temp = getHeroArea();
	Rect hero_temp2 = hero2->getHeroArea();

	if (hero_temp.intersectsRect(hero_temp2))
	{
		if (hero->getPositionY() > hero2->getPositionY())
		{
			if ((hero->getPositionX() - hero2->getPositionX()) < (hero2->getContentSize().width - hero->getContentSize().width / 2) && (hero->getPositionX() - hero2->getPositionX()) > -(hero->getContentSize().width / 2))
			{
				velocity.y = 0;
				acceleration.y = 0;
				hero->setPositionY(hero2->getPositionY() + hero2->getContentSize().height);
				IsJump = false;
			}
			else if ((hero->getPositionX() - hero2->getPositionX()) < -(hero->getContentSize().width / 2))
			{
				acceleration.x = 0;
				hero->setPositionX(hero2->getPositionX() - (hero->getContentSize().width));
			}
			else if ((hero->getPositionX() - hero2->getPositionX()) >(hero2->getContentSize().width - hero->getContentSize().width / 2))
			{
				acceleration.x = 0;
				hero->setPositionX(hero2->getPositionX() + hero2->getContentSize().width);
			}
		}
		else
		{
			if ((hero->getPositionX() - hero2->getPositionX()) < (hero2->getContentSize().width - hero->getContentSize().width / 2) && (hero->getPositionX() - hero2->getPositionX()) > -(hero->getContentSize().width / 2))
			{
				velocity.y = -velocity.y;
				hero->setPositionY(hero2->getPositionY() - hero->getContentSize().height);
			}
			else if ((hero->getPositionX() - hero2->getPositionX()) < -(hero->getContentSize().width / 2))
			{
				acceleration.x = 0;
				hero->setPositionX(hero2->getPositionX() - (hero->getContentSize().width));
			}
			else if ((hero->getPositionX() - hero2->getPositionX()) >(hero2->getContentSize().width - hero->getContentSize().width / 2))
			{
				acceleration.x = 0;
				hero->setPositionX(hero2->getPositionX() + hero2->getContentSize().width);
			}
		}
	}
}

Rect Hero::getHeroArea() const
{
	return Rect(getPositionX(), getPositionY(), getContentSize().width, getContentSize().height);
}

Rect Hero::getHeroRightBox() const
{
	return RightBox;
}

Rect Hero::getHeroLeftBox() const
{
	return LeftBox;
}

